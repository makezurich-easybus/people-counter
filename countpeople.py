#!/usr/bin/env python

'''
example to detect upright people in images using HOG features
Usage:
    peopledetect.py <image_names>
Press any key to continue, ESC to stop.
'''

# Python 2/3 compatibility
from __future__ import print_function

import numpy as np
import cv2 as cv


def inside(r, q):
    rx, ry, rw, rh = r
    qx, qy, qw, qh = q
    return rx > qx and ry > qy and rx + rw < qx + qw and ry + rh < qy + qh


def draw_detections(img, rects, thickness = 1):
    for x, y, w, h in rects:
        # the HOG detector returns slightly larger rectangles than the real objects.
        # so we slightly shrink the rectangles to get a nicer output.
        pad_w, pad_h = int(0.15*w), int(0.05*h)
        cv.rectangle(img, (x+pad_w, y+pad_h), (x+w-pad_w, y+h-pad_h), (0, 255, 0), thickness)


if __name__ == '__main__':
    import sys
    from glob import glob
    import itertools as it

    print(__doc__)
    
    #setting up the haar cascade classifiers from the opencv installation
    face_cascade = cv.CascadeClassifier('E:/opencv/build/etc/haarcascades/haarcascade_frontalface_default.xml') #testing with absolute paths
    body_cascade = cv.CascadeClassifier('E:/opencv/build/etc/haarcascades/haarcascade_upperbody.xml')

    #setting up HoG
    hog = cv.HOGDescriptor()
    hog.setSVMDetector( cv.HOGDescriptor_getDefaultPeopleDetector() )

        #Get input image(s)
    default = ['F0MW35.jpg'] if len(sys.argv[1:]) == 0 else []
    for fn in it.chain(*map(glob, default + sys.argv[1:])):
        print(fn, ' - ',)
        try:
            img = cv.imread(fn)
            if img is None:
                print('Failed to load image file:', fn)
                continue
        except:
            print('loading error')
            continue

        
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)     
        thresh = cv.threshold(img, 80, 250, cv.THRESH_BINARY)[1] #to use after background elimination?

        #HoG analysis
        #found, w = hog.detectMultiScale(gray, winStride=(8,8), padding=(16,16), scale=1.1)        
        
        #haar cascade analysis
        found = face_cascade.detectMultiScale(gray, 1.06, 0, 0, (4, 4), (800, 800))
        #found = body_cascade.detectMultiScale(img, 1.12, 0, 0, (4, 4), (1200, 1200))
        
        #Process results
        found_filtered = []
        print(found)
        
        for ri, r in enumerate(found):
            for qi, q in enumerate(found):
                if ri != qi and inside(r, q):
                    break
            else:
                found_filtered.append(r)
        draw_detections(img, found)
        draw_detections(img, found_filtered, 3)
        print('%d (%d) found' % (len(found_filtered), len(found)))
        cv.imshow('img', img)
        ch = cv.waitKey()
        if ch == 27:
            break
    cv.destroyAllWindows()